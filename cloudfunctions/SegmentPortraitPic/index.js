// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
  env: 'test-5r76l',
})

// Depends on tencentcloud-sdk-nodejs version 4.0.3 or higher
const tencentcloud = require("tencentcloud-sdk-nodejs");
const BdaClient = tencentcloud.bda.v20200324.Client;

/**
 * secretId和secretKey获取地址
 * https://console.cloud.tencent.com/cam/capi
 */
const clientConfig = {
  credential: {
    secretId: "去腾讯云复制",
    secretKey: "去腾讯云复制",
  },
  region: "ap-guangzhou",
  profile: {
    httpProfile: {
      endpoint: "bda.tencentcloudapi.com",
    },
  },
};

const client = new BdaClient(clientConfig);

// 云函数入口函数
exports.main = async (event, context) => {
  console.log(event.image)
  let params = {
    "Url": event.image
  }
  /**
   * 二分类人像分割接口开通地址
   * https: //console.cloud.tencent.com/bda/segment-portrait-pic
   * 文档地址
   * https://console.cloud.tencent.com/api/explorer?Product=bda&Version=2020-03-24&Action=SegmentPortraitPic&SignVersion=
   */
  let ResultImage = await client.SegmentPortraitPic(params).then(
    (data) => {
      // console.log(data);
      return data.ResultImage
    },
    (err) => {
      // console.error("error", err);
    }
  );
  // console.log(ResultImage)
  // console.log(new Buffer(ResultImage, 'base64'))
  const cloudPath = 'imageSegmentation/' + parseInt(new Date().getTime() / 1000) + '.jpg';
  // console.log('自定义文件名：' + cloudPath);
  return await cloud.uploadFile({
    cloudPath: cloudPath,
    fileContent: new Buffer(ResultImage, 'base64'),
  })
}