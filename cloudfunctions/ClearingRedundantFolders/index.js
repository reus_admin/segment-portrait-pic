// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
  env: 'test-5r76l',
})

const CloudBase = require('@cloudbase/manager-node')

/**
 * secretId和secretKey获取地址
 * https://console.cloud.tencent.com/cam/capi
 */
const {
  storage
} = new CloudBase({
  secretId: "去腾讯云复制",
  secretKey: "去腾讯云复制",
  envId: "test-5r76l", // 云开发环境ID，可在腾讯云云开发控制台获取
});

// 云函数入口函数
exports.main = async (event, context) => {
  return await Promise.all([
    storage.deleteDirectory("simpleCrop"),
    storage.deleteDirectory("imageSegmentation"),
    storage.deleteDirectory("IDPhoto")
  ])
}