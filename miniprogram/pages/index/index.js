Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 尺寸数组
    radioItems: [{
        id: '0',
        name: '一寸',
        size: '25*35mm',
        widthPX: 295,
        heightPX: 413,
        checked: true
      },
      {
        id: '1',
        name: '二寸',
        size: '35*49mm',
        widthPX: 413,
        heightPX: 579,
        checked: false
      },
      {
        id: '2',
        name: '小一寸',
        size: '22*32mm',
        widthPX: 260,
        heightPX: 378,
        checked: false
      },
      {
        id: '3',
        name: '小二寸',
        size: '35*45mm',
        widthPX: 413,
        heightPX: 531,
        checked: false
      },

    ],
    // 选中的像素尺寸，默认一寸
    widthPX: 295,
    heightPX: 413,
    /*****simple-cropc截图插件*****/
    src: null, // 选择图片后用于裁剪的图片
    visible: false, // 是否显示插件
    size: {
      width: 295*3, // 截取图片的宽
      height: 413*3 // 截取图片的高
    },
    cropSizePercent: 0.7, // 缩放比例
    borderColor: '#0f0', // 截取框的颜色
    result: '',
    /*****simple-cropc截图插件*****/
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  // 点击选择尺寸
  radioChange(e) {
    let index = e.detail.value;
    // console.log(index)
    let radioItem = this.data.radioItems[index];
    // console.log(radioItem)
    let size = {
      width: radioItem.widthPX*3,
      height: radioItem.heightPX*3
    }
    this.setData({
      size: size,
      widthPX: radioItem.widthPX,
      heightPX: radioItem.heightPX
    })
  },

  // 选取裁剪图片
  chooseCropImage: function () {
    let self = this;
    wx.chooseImage({
      sizeType: ["compressed"], // ios 选择原图容易 crash
      success(res) {
        // console.log(res)
        self.setData({
          visible: true,
          src: res.tempFilePaths[0]
        })
      }
    });
  },

  // 选取裁剪图片成功回调
  uploadCallback: function (event) {
    // console.log(event);
  },

  // 裁剪图片回调
  cropCallback: function (event) {
    // console.log(event);
    this.setData({
      visible: false,
      // result: event.detail.resultSrc
    });
    // 截取成功后插件返回的临时路径
    let resultSrc = event.detail.resultSrc;
    let widthPX = this.data.widthPX;
    let heightPX = this.data.heightPX;
    wx.navigateTo({
      url: `./create/create?resultSrc=${resultSrc}&widthPX=${widthPX}&heightPX=${heightPX}`,
    })
  },

  // 关闭回调
  closeCallback: function (event) {
    // console.log(event);
    this.setData({
      visible: false,
    });
  },


})